// 2015110705 김민규
// 본인은 이 소스파일을 복사하지 않고 직접 작성하였습니다.

#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>

#define MAX_STACKS 3

typedef struct {
	int id;
	int grade;
} element;

typedef struct stack* stackPointer;
typedef struct stack {
	element data;
	stackPointer link;
} Node;

stackPointer top[MAX_STACKS];

void push(int i, element item);
element pop(int i);
element stackEmpty();
void printStack(int i);

int main()
{
	FILE *pInputFile = fopen("input.txt", "r");
	element temp;
	int buffer;
	int i;

	for (i = 0; i < MAX_STACKS; i++) {
		top[i] = NULL;
	}

	//파일 입출력
	fscanf(pInputFile, "%d %d %d", &buffer, &(temp.id), &(temp.grade));
	while (!feof(pInputFile)) {
		push(buffer, temp);
		fscanf(pInputFile, "%d %d %d", &buffer, &(temp.id), &(temp.grade));
	}

	fclose(pInputFile);

	printf("과목번호, 학번, 성적 \n");
	for (i = 0; i < MAX_STACKS; i++) {
		if (top[i] == NULL) {
			continue;
		}
		printStack(i);
	}

	return 0;
}

void push(int i, element item)
{
	stackPointer temp = (stackPointer)malloc(sizeof(Node));
	temp->data = item;
	temp->link = top[i];
	top[i] = temp;
}

element pop(int i)
{
	stackPointer temp = top[i];
	element item;
	if (!temp) {
		return stackEmpty();
	}
	item = temp->data;
	top[i] = temp->link;
	free(temp);
	return item;
}

element stackEmpty()
{
	fprintf(stderr, "Stack is Empty \n");
	exit(0);
}

void printStack(int i)
{
	stackPointer temp = top[i];

	printf("***********************************\n");
	while (temp != NULL) {
		printf("%8d%6d%6d\n", i ,(temp->data).id, (temp->data).grade);
		temp = temp->link;
	}
}