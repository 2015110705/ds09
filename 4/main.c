// 2015110705 김민규
// 본인은 이 소스파일을 복사하지 않고 직접 작성하였습니다.

#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>

#define MAX_QUEUES 3

typedef struct {
	int id;		//학번
	int grade;	//성적
} element;

typedef struct queue *queuePointer;
typedef struct queue {
	element data;
	queuePointer link;
}Node;

queuePointer front[MAX_QUEUES], rear[MAX_QUEUES];

void addq(int i, element item);
element deleteq(int i);
element queueEmpty();
void printQueue(int i);

int main()
{
	FILE *pInputFile = fopen("input.txt", "r");
	element temp;
	int buffer;
	int i;

	for (i = 0; i < MAX_QUEUES; i++) {
		front[i] = NULL;
		rear[i] = NULL;
	}

	//파일 입출력
	fscanf(pInputFile, "%d %d %d", &buffer, &(temp.id), &(temp.grade));
	while (!feof(pInputFile)) {
		addq(buffer, temp);
		fscanf(pInputFile, "%d %d %d", &buffer, &(temp.id), &(temp.grade));
	}

	fclose(pInputFile);

	printf("과목번호, 학번, 성적 \n");
	for (i = 0; i < MAX_QUEUES; i++) {
		if (front[i] == NULL) {
			continue;
		}
		printQueue(i);
	}

	return 0;
}

void addq(int i, element item)
{
	queuePointer temp;
	temp = (queuePointer)malloc(sizeof(Node));
	temp->data = item;
	temp->link = NULL;
	if (front[i]) {
		rear[i]->link = temp;
	}
	else {
		front[i] = temp;
	}
	rear[i] = temp;
}

element deleteq(int i)
{
	queuePointer temp = front[i];
	element item;
	if (!temp) {
		return queueEmpty();
	}
	item = temp->data;
	front[i] = temp->link;
	free(temp);
	return item;
}

element queueEmpty()
{
	fprintf(stderr, "Queue is Empty \n");
	exit(0);
}

void printQueue(int i)
{
	queuePointer temp = front[i];

	printf("***********************************\n");
	while (temp != NULL) {
		printf("%8d%6d%6d\n", i, (temp->data).id, (temp->data).grade);
		temp = temp->link;
	}
}