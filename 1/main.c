// 2015110705 김민규
// 본인은 이 소스파일을 복사하지 않고 직접 작성하였습니다.

#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>

typedef struct listNode* listPointer;
typedef struct listNode {
	int data;
	struct listNode *link;
} listNode;

void deleteNode(listPointer* head, listPointer prevNode, listPointer removeNode);
void insertNode(listPointer* head, listPointer prevNode, int data);
listPointer find(listPointer head, int data);
void printList(listPointer head);

int main()
{
	FILE *pInputFile = fopen("input.txt", "r");
	listNode *head = NULL;
	listNode *temp = head;
	int buffer;

	fscanf(pInputFile, "%d", &buffer);
	while (!feof(pInputFile)) {
		//삽입
		insertNode(&head, find(head, buffer), buffer);

		//다시 파일에서 데이터 입력
		fscanf(pInputFile, "%d", &buffer);
	}
	fclose(pInputFile);

	//출력 
	printList(head);

	//50이하 전부 삭제
	while (head->data <= 50) {
		deleteNode(&head, NULL, head);
	}

	//출력 
	printf("After deleting nodes with data less than and equal to 50 \n");
	printList(head);

	return 0;
}

void deleteNode(listPointer* head, listPointer prevNode, listPointer removeNode)
{
	//prevNode 뒤에 있는 removeNode를 삭제
	if (prevNode) {
		prevNode->link = removeNode->link;
	}
	else {
		*head = (*head)->link;
	}
	free(removeNode);
}

void insertNode(listPointer* head, listPointer prevNode, int data)
{
	//1. insert가 헤드일 경우
	//2. insert가 헤드 앞일 경우
	//3. 나머지 경우

	//새로운 노드 추가 및 초기화
	listPointer newNode = (listPointer)malloc(sizeof(listNode));
	listPointer temp;

	newNode->data = data;
	newNode->link = NULL;

	//경우 1
	if (*head == NULL) {
		*head = newNode;
	}
	else {
		//경우 2
		if (prevNode->data > data) {
			temp = *head;
			*head = newNode;
			newNode->link = temp;
		}
		//경우 3
		else {
			newNode->link = prevNode->link;
			prevNode->link = newNode;
		}
	}
}

listPointer find(listPointer head, int data)
{
	listPointer temp = head;

	if (temp == NULL)
		return NULL;

	while (temp->link != NULL && temp->link->data < data) {
		temp = temp->link;
	}

	return temp;
}

void printList(listPointer head)
{
	listPointer temp = head;
	int escape = 1;

	printf("The ordered List contains : \n");

	while (temp != NULL) {
		printf("%4d ", temp->data);
		temp = temp->link;

		//띄어쓰기
		if (escape % 10 == 0) {
			printf("\n");
		}
		escape++;
	}
	printf("\n");
}